# Async-Await-Practice

Usecase and pracitice for Await/Async with typescript

This class is an example and training for people who want to deep diver or start into async / await
It's completly commented. You can run and play around with it.

If you want to dive deeper you can google terms like..
"combine promises with async / await"
"listener with async / await"
"emitter with async / await"

Dry run without build: node .\dist\index.js

Have fun with it :)
greets Alexander Jungert

licence: GNU GPLv3

feel free to share it.

ps.: dont forget to delete "format": "prettier" in package.json if you dont use it.
