"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const perf_hooks_1 = require("perf_hooks");
const TIMER_PERFOMANCE_LOG = false;
function LtLog(msg) {
    console.log(TimeNow() + 'Lifetime: ' + msg);
}
exports.LtLog = LtLog;
function TimeNow() {
    let start = 0, end = 0;
    try {
        if (TIMER_PERFOMANCE_LOG)
            start = perf_hooks_1.performance.now();
        let d = new Date();
        let a = d.toLocaleTimeString() + ':' + d.getMilliseconds() + ' ';
        if (TIMER_PERFOMANCE_LOG)
            end = perf_hooks_1.performance.now();
        if (TIMER_PERFOMANCE_LOG)
            console.log('Time task took: ' + (end - start) + ' ms to execute');
        return a;
    }
    catch (err) {
        console.log(err);
        return '##/ ';
    }
}
exports.TimeNow = TimeNow;
//# sourceMappingURL=logger.js.map