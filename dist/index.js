"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const PerfomanceLogger_1 = require("./helper/PerfomanceLogger");
let counter;
/**Standard function like you know it */
function StandardGain() {
    PerfomanceLogger_1.LtLog('Run Test1() standard function return 1');
    return 1;
}
/**
 * This is an example how setTimeout inside a async function will be computed
 * @param Value any number
 * @param waitms delay in ms
 */
function asyncRaiseValue(Value, waitms) {
    return __awaiter(this, void 0, void 0, function* () {
        PerfomanceLogger_1.LtLog('Time out set to: ' + waitms + 'ms Test2()(Value +=1) -> Value:' + Value);
        setTimeout(() => {
            Value += 1; //this will NOT raise the value after waitms ms!
            //if u want to raise it, do it with a callback.
            PerfomanceLogger_1.LtLog('inside timeout after ' + waitms + 'ms Test2()(Value +=1) -> Value:' + Value);
        }, waitms);
        //this Log won't be delayed because it's outside the time out
        PerfomanceLogger_1.LtLog('Value after setTimeout: ' + Value);
        return 1; //this will be directly returned because it's outside the time out and an instant.
    });
}
/**
 * Async Dummy blocker for test purposes
 * @param iterator 0 - ?
 * @param target 1e5 - 6e9 for testing
 */
function simulatedLongTask(iterator, target) {
    return __awaiter(this, void 0, void 0, function* () {
        let i;
        try {
            PerfomanceLogger_1.LtLog('starting async looper -> target ' + target);
            i = yield looper(iterator, target);
        }
        catch (error) {
            console.log(error);
            return 0;
        }
        return i;
    });
}
/**
 * Loops
 * @param iterator 0 - ?
 * @param target 1e5 - 6e9 for testing
 */
function looper(iterator, target) {
    PerfomanceLogger_1.performanceLog.pLogStart();
    do {
        iterator++;
    } while (iterator < target);
    PerfomanceLogger_1.performanceLog.pLogStop('looper()');
    return iterator;
}
/**Function must be async to run await orders */
function run() {
    return __awaiter(this, void 0, void 0, function* () {
        counter = 0;
        let shoutIterator = 1;
        PerfomanceLogger_1.LtLog('Value on start after set: ' + counter);
        PerfomanceLogger_1.LtLog('Shout ' + shoutIterator++ + ' for order look up');
        counter += StandardGain();
        PerfomanceLogger_1.LtLog('Value after Test1(): ' + counter);
        PerfomanceLogger_1.LtLog('Shout ' + shoutIterator++ + ' for order look up');
        counter += yield asyncRaiseValue(counter, 500); //<-- this runs completly async and tries to fullfill its setTimeout based on threadcycle.  v will be raised by return
        //<-- it also ignores the execution order completely because there is no compute time till return; see-> test2()
        //<-- if the processor got blocked in this time the fullfillment will come after the block
        PerfomanceLogger_1.LtLog('Value after v= await Test2(): ' + counter);
        PerfomanceLogger_1.LtLog('Shout ' + shoutIterator++ + ' for order look up');
        asyncRaiseValue(counter, 3000); //<-- this runs completly async and tries to fullfill its setTimeout. v will not be raised because we dont catch the return
        //<-- it also ignores the execution order completely because there is no compute time till return; see-> test2()
        //<-- if the processor got blocked in this time the fullfillment will come after the block
        PerfomanceLogger_1.LtLog('Value after Test2(): ' + counter);
        PerfomanceLogger_1.LtLog('Shout ' + shoutIterator++ + ' for order look up');
        PerfomanceLogger_1.LtLog('Start wait task..');
        let i = yield simulatedLongTask(0, 6e9); //<-- simulates timer task we want to wait for
        PerfomanceLogger_1.LtLog('Wait task end..'); //<-- execute order is still intact because of the correct usage of await see -> waitforit() is an example
        let x = yield simulatedLongTask(0, 3e9); //<-- simulates timer task we want to wait for
        counter += yield asyncRaiseValue(counter, 4000); //<-- this runs completly async and tries to fullfill its setTimeout based on threadcycle. v will be raised by return
        //<-- it also ignores the execution order completely because there is no compute time till return; see-> test2()
        //<-- if the processor got blocked in this time the fullfillment will come after the block
        yield simulatedLongTask(0, 2e9); // the last v += await Test2(v, 4000); will be on time. look logs "Time out set to: 4000ms"
        //and for approve: <time> << Lifetime: inside timeout after 4000ms
        PerfomanceLogger_1.LtLog(counter); // this will be logged before the last v += await Test2(v, 4000);
    });
}
run();
//# sourceMappingURL=index.js.map