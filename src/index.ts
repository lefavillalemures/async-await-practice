import {LtLog, performanceLog} from './helper/PerfomanceLogger';

let counter: number;
/**
 * This class is an example and training for people who want to deep diver or start into async / await
 * It's completly commented. You can run and play around with it.
 *
 * If you want to dive deeper you can google terms like..
 * "combine promises with async / await"
 * "listener with async / await"
 * "emitter with async / await"
 *
 * Have fun with it :)
 * greets @author Alexander Jungert
 */

/**Standard function like you know it */
function StandardGain(): number {
    LtLog('Run Test1() standard function return 1');
    return 1;
}

/**
 * This is an example how setTimeout inside a async function will be computed
 * @param Value any number
 * @param waitms delay in ms
 */
async function asyncRaiseValue(Value: number, waitms: number) {
    LtLog('Time out set to: ' + waitms + 'ms Test2()(Value +=1) -> Value:' + Value);
    setTimeout(() => {
        Value += 1; //this will NOT raise the value after waitms ms!
        //if u want to raise it, do it with a callback.
        LtLog('inside timeout after ' + waitms + 'ms Test2()(Value +=1) -> Value:' + Value);
    }, waitms);
    //this Log won't be delayed because it's outside the time out
    LtLog('Value after setTimeout: ' + Value);
    return 1; //this will be directly returned because it's outside the time out and an instant.
}

/**
 * Async Dummy blocker for test purposes
 * @param iterator 0 - ?
 * @param target 1e5 - 6e9 for testing
 */
async function simulatedLongTask(iterator: number, target: number) {
    let i;
    try {
        LtLog('starting async looper -> target ' + target);
        i = await looper(iterator, target);
    } catch (error) {
        console.log(error);
        return 0;
    }
    return i;
}
/**
 * Loops
 * @param iterator 0 - ?
 * @param target 1e5 - 6e9 for testing
 */
function looper(iterator: number, target: number) {
    performanceLog.pLogStart();
    do {
        iterator++;
    } while (iterator < target);
    performanceLog.pLogStop('looper()');
    return iterator;
}

/**Function must be async to run await orders */
async function run() {
    counter = 0;
    let shoutIterator = 1;
    LtLog('Value on start after set: ' + counter);
    LtLog('Shout ' + shoutIterator++ + ' for order look up');
    counter += StandardGain();
    LtLog('Value after Test1(): ' + counter);
    LtLog('Shout ' + shoutIterator++ + ' for order look up');
    counter += await asyncRaiseValue(counter, 500); //<-- this runs completly async and tries to fullfill its setTimeout based on threadcycle.  v will be raised by return
    //<-- it also ignores the execution order completely because there is no compute time till return; see-> test2()
    //<-- if the processor got blocked in this time the fullfillment will come after the block
    LtLog('Value after v= await Test2(): ' + counter);
    LtLog('Shout ' + shoutIterator++ + ' for order look up');
    asyncRaiseValue(counter, 3000); //<-- this runs completly async and tries to fullfill its setTimeout. v will not be raised because we dont catch the return
    //<-- it also ignores the execution order completely because there is no compute time till return; see-> test2()
    //<-- if the processor got blocked in this time the fullfillment will come after the block
    LtLog('Value after Test2(): ' + counter);
    LtLog('Shout ' + shoutIterator++ + ' for order look up');
    LtLog('Start wait task..');
    let i = await simulatedLongTask(0, 6e9); //<-- simulates timer task we want to wait for
    LtLog('Wait task end..'); //<-- execute order is still intact because of the correct usage of await see -> waitforit() is an example
    let x = await simulatedLongTask(0, 3e9); //<-- simulates timer task we want to wait for
    counter += await asyncRaiseValue(counter, 4000); //<-- this runs completly async and tries to fullfill its setTimeout based on threadcycle. v will be raised by return
    //<-- it also ignores the execution order completely because there is no compute time till return; see-> test2()
    //<-- if the processor got blocked in this time the fullfillment will come after the block
    await simulatedLongTask(0, 2e9); // the last v += await Test2(v, 4000); will be on time. look logs "Time out set to: 4000ms"
    //and for approve: <time> << Lifetime: inside timeout after 4000ms
    LtLog(counter); // this will be logged before the last v += await Test2(v, 4000);
}

run();
