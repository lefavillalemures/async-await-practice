import {performance} from 'perf_hooks';

/**
 * set true if u want to know the process time for the logout
 */
const TIMER_PERFOMANCE_LOG: boolean = false;
/**
 * Log msg with time
 * @param msg any msg
 */
export function LtLog(msg: any) {
    console.log(TimeNow() + 'Lifetime: ' + msg);
}
/**
 * The time now displayed as hh/mm/ss/ms
 */
export function TimeNow() {
    let start = 0,
        end = 0;
    try {
        if (TIMER_PERFOMANCE_LOG) start = performance.now();
        let d = new Date();
        let a: string = d.toLocaleTimeString() + ':' + d.getMilliseconds() + ' ';
        if (TIMER_PERFOMANCE_LOG) end = performance.now();
        if (TIMER_PERFOMANCE_LOG) console.log('Time task took: ' + (end - start) + ' ms to execute');
        return a;
    } catch (err) {
        console.log(err);
        return '##/ ';
    }
}
/**
 * @tutorial Tutorial
 * @function pLogStart place where you want to start the log (any function or code)
 * @function pLogStop place where you want to stop the log (any function or code)
 */
export namespace performanceLog {
    let pstart: number = 0;
    let pend: number = 0;
    /**
     * place it above the code where you want to start to measure
     */
    export function pLogStart() {
        pstart = performance.now();
    }
    /**
     * place it after the code where you want to stop and log the measure
     * @param msg
     */
    export function pLogStop(msg: any) {
        pend = performance.now();
        LtLog(msg + ' ' + (pend - pstart) + ' ms to execute');
    }
}
